/*
 * Created by B00102152
 */


var divID = 0;
var cbuttonID = 0;
var dbuttonID = 1000;
var nametoText;


var Fault = function(employeename, datetime, location, category, notes, contactno) {
    this.employeename = employeename;
    this.datetime = datetime;
	this.location = location;
    this.category = category;
    this.notes = notes;
    this.contactno = contactno;
};

var faultsArray = [];
var myControls = [];

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};


/**
 * Create divs to show all of the recorded faults stored in localStorage.
 */
Fault.prototype.divRow = function(){

    divID++;
    cbuttonID--;
    dbuttonID++;
    var control = [];
    control[0] = divID;
    control[1] = cbuttonID;
    var div;
    myControls.push(control);

    if (this.employeename === "") {

        this.employeename = "-";
    }


    if (this.datetime == "") {

        this.datetime = "-";
    }

    if (this.category === "" ) {

        this.category = "-";
    }

    if (this.notes === ""){

        this.company = "-";
    }

    if (this.contactno === "") {

        this.contactno = "-";
    }
	
	div = "</br>" +

	"<div class='parentDiv'> <span class='nameHeading'><b id='nameforText'>" + this.category + ", logged on </br>" +
	this.datetime + "</span></b>" + "</br></br><button class='DivExpand' id='"+cbuttonID+"'>Details</button>" + "<button class='deleteContact' id='"+dbuttonID+"'>Delete</button>" +
	"<div class='childDiv_1' id='" + divID + "'>" +
	"<div id='childDiv_2'>" + "<b>Employee Name: </b></br> "  + this.employeename + "</br></br><b>Employee Contact No: </b></br> " + this.contactno + "</br></br> <b>Category: </b>" + this.category +
	"</br></br> <b>Location: </b>" + this.location + "<button onclick='openBrowser(&quot;"+this.location+"&quot;)'>Open Map</button>" +
	"</br></br><b>Notes:</b> </br>" +
	this.notes +
	"</div>" + "</div>" + "</div>";

	return div;
};

function saveFaultList() {
    var faultArrayString = JSON.stringify(faultsArray);
    if(faultArrayString !== ""){
        localStorage.faults = faultArrayString;
    } else {
        alert("Could not save the fault at this time");
    }
}

var loadFaultList = function() {
    var faultArrayString = "";
    if(localStorage.faults !== undefined) {
        faultArrayString = localStorage.faults;
        faultsArray = JSON.parse(faultArrayString);
		var proto = new Fault();
		for(var i=0;i<faultsArray.length;i++) {
            var flt = faultsArray[i];
			flt.__proto__ = proto;
		}
	}
}

var updateList = function(){
    var divList = document.getElementById("faultList");
    divList.innerHTML = "";

    for(var i=0, j=faultsArray.length; i<j; i++){
        var fault = faultsArray[i];
        divList.innerHTML += fault.divRow();
    }
};

var openBrowser = function(location) {
	cordova.InAppBrowser.open('http://maps.google.co.uk/maps?q=' + location, '_blank', 'location=yes');
}

/**
 * Takes a variable corresponding to a position within the faults array, then deletes (splices) that entry.
 * saveFaultList is then called to match the altered array to local storage.
 * @param int
 */
function removeFault (int) {

    faultsArray.splice(int, 1);
    divID = 0;
    cbuttonID = 0;
    dbuttonID = 1000;
    myControls = [];
    saveFaultList();
    updateList();

}

window.onload = function(){
    loadFaultList();
    updateList();
};


/**
 * This function takes the id of the Details button, and matches it with the ID of the div it is held within. By
 * doing this we can use this button to extend the div when the user clicks the button, allowing for a neat list
 * of faults in faults.html.
 */
$(document).on('click','.DivExpand',function(){

    var id = $(this).prop("id");
    var divid;
//Now it matches the button to the DIV
    for (var i = 0; i < myControls.length; i++){
        if (myControls[i][1] == id){
            divid = myControls[i][0];
            break;
        }
    }

// Toggles the DIV
    $("#" + divid).slideToggle('fast');
});

/**
 * This function will grab the id of the delete button. As each delete button ID starts from 1001, the ID is decremented
 * by 1001 to give the div ID that it corresponds to. This allows the removeFault function to delete the fault.
 */
$(document).on('click','.deleteContact',function(){
    var response = confirm("Are you sure you want to delete this fault?");

    if (response) {
        var divid = $(this).prop("id");
        divid = divid - 1001;

        removeFault(divid);
    }
});
