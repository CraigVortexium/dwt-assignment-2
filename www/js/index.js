/*
 * Created by B00102152.
 */


/* Below we declare some variables used in the application.*/

var entry;

var employeenameField, datetimeField, locationField, categoryField, notesField, contactnoField;

var Fault = function(employeename, datetime, location, category, notes, contactno) {
    this.employeename = employeename;
    this.datetime = datetime;
	this.location = location;
    this.category = category;
    this.notes = notes;
    this.contactno = contactno;
};

// Set up the array to store our faults.

var faultsArray = [];

/**
 * Function to convert the date into a readable
 * string.
 * @returns {string}
 */

Fault.prototype.getDate = function(){
    return this.dob.toDateString() ;
};

/**
 * Function to capitalise the first letter of a string and return
 * the result.
 * @returns {string}
 */

String.prototype.capitalize = function() {
    if (this.String() !== undefined)
    return this.charAt(0).toUpperCase() + this.slice(1);
};


var addFault = function(employeenameField, datetimeField, locationField, categoryField, notesField, contactnoField) {

    entry = new Fault(employeenameField.value, datetimeField.value, locationField.value, categoryField.value, notesField.value, contactnoField.value);
    faultsArray.push(entry);
};

var add = function () {
    addFault(employeenameField, datetimeField, locationField, categoryField, notesField, contactnoField);
    clearUI();
    saveFaultList();
};

// Function to clear all UI fields.

var clearUI = function() {

    employeenameField.value = "";
	locationField.value = "";
    categoryField.value = "";
    notesField.value = "";
    contactnoField.value = "";
};


window.onload = function(){
	

    employeenameField = document.getElementById("employeename");
    datetimeField = document.getElementById("datetime");
	locationField = document.getElementById("location");
    categoryField = document.getElementById("category");
    notesField = document.getElementById("notes");
    contactnoField = document.getElementById("contactno");

    resetButton = document.getElementById("reset");
    resetButton.onclick = clearUI;

	submitButton = document.getElementById("submit");
    submitButton.onclick = function(){

        var response = confirm("Add this fault to the list?");

        if (response) {
            add();
            alert("Fault added.");
		}
	}
	
	loadFaultList();
    clearUI();

};

function saveFaultList() {
    var faultArrayString = JSON.stringify(faultsArray);
    if(faultArrayString !== ""){
        localStorage.faults = faultArrayString;
    } else {
        alert("Could not save the fault at this time");
    }
}

var loadFaultList = function() {
    var faultArrayString = "";
    if(localStorage.faults !== undefined) {
        faultArrayString = localStorage.faults;
        faultsArray = JSON.parse(faultArrayString);
		var proto = new Fault();
		for(var i=0;i<faultsArray.length;i++) {
            var flt = faultsArray[i];
			flt.__proto__ = proto;
		}
	}
}

// onSuccess Callback
// This method accepts a Position object, which contains the
// current GPS coordinates
//
var onSuccess = function(position) {
    $("#location").val("loc:" + position.coords.latitude + "+" + position.coords.longitude);
};

// onError Callback receives a PositionError object
//
function onError(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}

$(document).ready(function(){

   $('#datetime').val(new Date($.now()));
   // When geoButton is clicked
    $("#geoButton").click(function () {
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
    });

});
